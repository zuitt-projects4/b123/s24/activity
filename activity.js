db.users.find({$or:[{firstName: {$regex: 'a', $options: '$i'}},{lastName: {$regex: 'a', $options: '$i'}}]},{_id:0, email:1, isAdmin:1})

db.users.find({$and:[{firstName: {$regex: 'i', $options: '$i'}},{isAdmin:true}]},{_id:0, email:1, isAdmin:1})

db.products.find({$and:[{name: {$regex: 'x', $options:'$i'}},{price:{$gte:50000}}]})

db.products.updateMany({price: {$lt:1000}},{$set:{isActive:false}})

db.users.deleteMany({email:{$regex: 'tech', $options: '$i'}})

